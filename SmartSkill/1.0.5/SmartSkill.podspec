Pod::Spec.new do |spec|

  spec.name         = "SmartSkill"
  spec.version      = "1.0.5"
  spec.summary      = "The iOS library specification for SmartSkill Framework."
  spec.description  = <<-DESC
  This is the iOS library for SmartSkill by Enparadigm
                   DESC
  spec.homepage     = "https://www.enparadigm.com"
  spec.license      = "ENPARADIGM"
  spec.author             = { "Meera Seetharam" => "meera.seetharam@enparadigm.com" }
  spec.platform     = :ios
  spec.ios.deployment_target = "9.0"
  spec.source       = { :git => "https://gitlab.com/enparadigm_public/smartskill-ios-framework.git", :tag => "#{spec.version}" }
#  spec.source_files  = "SmartSkill.framework/Headers/*.h"
  spec.vendored_frameworks = "SmartSkill.framework"
  spec.pod_target_xcconfig = { 'SWIFT_VERSION' => '5' }
 # spec.public_header_files = "SmartSkill.framework/Headers/*.h"
  spec.library   = "sqlite3"
  spec.requires_arc = true
  
  spec.preserve_paths = 'SmartSkill.framework'
  spec.module_map = 'SmartSkill.framework/Modules/module.modulemap'

  spec.dependency "SDWebImage"
  spec.dependency "Alamofire"
  spec.dependency "Kingfisher"
  spec.dependency "TTFortuneWheel"
  spec.dependency "DropDown"
  spec.dependency "Koloda"
  spec.dependency "Cartography"
  spec.dependency "MBRadioButton"
  spec.dependency "lottie-ios"
  spec.dependency "SwiftSVG"
  spec.dependency "JJFloatingActionButton"
  spec.dependency "YoutubePlayer-in-WKWebView"
  spec.dependency "KeychainSwift"

end
