# SmartSkill Framework Specs

This spec repo contains all the list of dependent pods for SmartSkill Framework,deployment target and swift version it supports. It is used for distributing the SmartSkill library as a framework.

Installation
1. Add the path of this podspec to your project pod file.
 `pod 'SmartSkill', :git => 'https://gitlab.com/enparadigm_public/smartskill_ios_podspec.git'`
2. Save your pod file.    
3. Then Run `pod install` command from your terminal.